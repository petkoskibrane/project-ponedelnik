<?php

use Illuminate\Database\Seeder;
use App\Movie;
use App\Director;
use App\Author;
class add_data_to_tables extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for ($i=0; $i<=100; $i++){
            $movie = new Movie();
            $movie->name = $faker->text(50);
            $movie->year = $faker->date();

            $movie->save();

            $movie2 = new Movie();
            $movie2->name = $faker->text(50);
            $movie2->year = $faker->date();

            $movie2->save();

            $director = new Director();

            $director->first_name = $faker->firstName();
            $director->last_name = $faker->firstName();

            $director->save();


            $author = new Author();

            $author->first_name = $faker->firstName();
            $author->last_name = $faker->firstName();
            $author->date_of_birth = $faker->date();
            $author->save();

            $author2 = new Author();

            $author2->first_name = $faker->firstName();
            $author2->last_name = $faker->firstName();
            $author2->date_of_birth = $faker->date();
            $author2->save();

            $author3 = new Author();

            $author3->first_name = $faker->firstName();
            $author3->last_name = $faker->firstName();
            $author3->date_of_birth = $faker->date();
            $author3->save();

            $director->movies()->saveMany([$movie,$movie2]);

            $movie->authors()->saveMany([$author,$author2,$author3]);

            $movie2->authors()->saveMany([$author,$author2,$author3]);



        }
    }
}
