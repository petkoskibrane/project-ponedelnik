<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/showForm')->name('show-form')->uses('MoviesController@showForm');

Route::post('/saveMovie')->name('save-movie')->uses('MoviesController@saveMovie');

Route::get('listMovies')->name('list-movies')->uses('MoviesController@showAllMovies');