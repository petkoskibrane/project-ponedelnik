<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    public function movie(){
        return $this->belongsToMany('App\Movie','authors_movies');
    }
}
