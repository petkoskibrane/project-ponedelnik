<?php

namespace App\Http\Controllers;

use App\Author;
use App\Director;
use App\Movie;
use Illuminate\Http\Request;

class MoviesController extends Controller
{
    public function showForm(){
        $directors = Director::all();

        $authors = Author::all();



        return view('add-form')->with(['directors' => $directors,'authors'=> $authors]);
    }


    public function showAllMovies(){



        return view('list-movies');
    }


    public function saveMovie(Request $request){
        $data = $request->all();

        $movie = new Movie();
        $movie->name = $data['name'];
        $movie->year = $data['year'];
        $movie->save();

        $author = Author::find($data['authors']);
        $director = Director::find($data['director']);
        $movie->authors()->save($author);
        $movie->director()->associate($director);

        return redirect()->route('list-movies');

    }
}
