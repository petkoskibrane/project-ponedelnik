@extends('defaultLayout')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <form action="{{route('save-movie')}}" method="post">

                {{csrf_field()}}

                <div class="form-group">
                    <label>Movie Name</label>
                    <input class="form-control" type="text" name="name"/>
                </div>
                <div class="form-group">
                    <label>Movie Year</label>
                    <input class="form-control" type="date" name="year"/>
                </div>
                <div class="form-group">
                    <label>Director</label>
                    <select class="form-control" name="director">
                        @foreach($directors as $director)
                            <option value="{{$director->id}}">{{$director->first_name ." ". $director->last_name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label>Authors</label>
                    <select class="form-control" name="authors">
                        @foreach($authors as $author)
                            <option value="{{$author->id}}">{{$author->first_name ." ". $author->last_name}}</option>

                        @endforeach
                    </select>

                    <br>
                </div>
                <div class="form-group">
                    <input class="btn btn-primary" type="submit" />
                </div>
            </form>
        </div>
    </div>
</div>


@endsection
