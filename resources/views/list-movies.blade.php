@extends('defaultLayout')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Year</th>
                        <th>Title</th>
                        <th>Option</th>
                    </tr>



                    </thead>
                    <tbody>

                        <tr>

                            <td class="col-md-2"></td>
                            <td class="col-md-7"></td>
                            <td class="col-md-3">
                                <div class="col-md-6">
                                    <a class="btn btn-primary" href="#">Details</a>
                                </div>
                                <div class="col-md-6">
                                    <form  method="post">
                                        {{csrf_field()}}
                                        <input hidden  value="delete" name="_method"/>
                                        <input class="btn btn-danger" type="submit" value="Delete"/>
                                    </form>

                                </div>


                            </td>
                        </tr>


                    </tbody>
                </table>

            </div>
        </div>

    </div>


@endsection